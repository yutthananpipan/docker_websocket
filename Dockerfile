FROM node:10.15.0-alpine

RUN apk update && \
apk upgrade

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm i

COPY . .

EXPOSE 8888

CMD ["node","index.js"]